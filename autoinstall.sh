#!/bin/bash
dir=$(realpath $(dirname $0))
cp ${dir}/.gitattributes ${dir}/.gitignore ${dir}/.gitlab-ci.yml .
rm -rf $dir